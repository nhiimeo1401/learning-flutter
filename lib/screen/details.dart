import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../variable/constants.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppBar buildAppBar() {
      return AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0,
        title: const Text('Dashboard'),
        centerTitle: false,
        actions: <Widget>[
          IconButton(
              onPressed: null,
              icon: SvgPicture.asset("assets/icons/notification.svg")),
        ],
      );
    }

    return Scaffold(
      backgroundColor: kPrimaryColor,
      appBar: buildAppBar(),
      body: Container(),
    );
  }
}
