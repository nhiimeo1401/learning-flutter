import 'package:flutter/material.dart';
import 'package:helloword/variable/constants.dart';
import 'package:helloword/screen/details.dart';
import 'package:helloword/models/product.dart';

import '../component/categories_list.dart';
import '../component/product_card.dart';
import '../component/search_box.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SearchBox(),
        const CategoryList(),
        const SizedBox(
          height: kDefaultPadding / 2,
        ),
        Expanded(
          child: Stack(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 70),
                decoration: const BoxDecoration(
                  color: kBackgroundColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
              ),
              ListView.builder(
                itemCount: products.length,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DetailsPage()),
                    );
                  },
                  child: ProductCard(
                    itemIndex: index,
                    product: products[index],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
