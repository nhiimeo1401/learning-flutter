import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../variable/constants.dart';

class SearchBox extends StatelessWidget {
  SearchBox({
    Key? key,
    this.onChanged,
  }) : super(key: key);

  final ValueChanged? onChanged;
  List item = ['Bình', 'Yêu', 'Vy', 'Bình xin lỗi'];

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(kDefaultPadding),
        padding: const EdgeInsets.symmetric(
            horizontal: kDefaultPadding, vertical: kDefaultPadding / 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white.withOpacity(0.4)),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 8),
              child: SizedBox(
                width: MediaQuery.of(context).size.width - 160,
                height: 60,
                child: TextField(
                  onChanged: onChanged,
                  style: const TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintText: 'Tìm kiếm',
                    hintStyle: const TextStyle(color: Colors.white),
                    icon: SvgPicture.asset("assets/icons/search.svg"),
                  ),
                ),
              ),
            ),
            DropdownButton(
              items: item.map((e) {
                return DropdownMenuItem(
                  child: Text(e),
                  value: e,
                );
              }).toList(),
              onChanged: (newValue) {},
              value: item[0],
              underline: const SizedBox(),
              style: TextStyle(color: Colors.orange),
              iconDisabledColor: Colors.orange,
            ),
          ],
        ));
  }
}

class Kieu1 extends StatelessWidget {
  const Kieu1({
    Key? key,
    required this.onChanged,
    required this.item,
  }) : super(key: key);

  final ValueChanged? onChanged;
  final List item;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      style: const TextStyle(color: Colors.white),
      decoration: InputDecoration(
        enabledBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
        hintText: 'Tìm kiếm',
        hintStyle: const TextStyle(color: Colors.white),
        icon: SvgPicture.asset("assets/icons/search.svg"),
        suffix: DropdownButton(
          items: item.map((e) {
            return DropdownMenuItem(
              child: Text(e),
              value: e,
            );
          }).toList(),
          onChanged: (newValue) {},
          value: item[0],
          underline: const SizedBox(),
          style: TextStyle(color: Colors.orange),
          iconDisabledColor: Colors.orange,
        ),
      ),
    );
  }
}
