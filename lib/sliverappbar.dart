import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/diagnostics.dart';
import 'package:helloword/component/product_card.dart';
import 'package:helloword/component/search_box.dart';
import 'package:helloword/details.dart';
import 'package:helloword/models/product.dart';

class Sliver extends StatelessWidget {
  const Sliver({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          title: const Text('Binh'),
          backgroundColor: Colors.transparent,
          pinned: true,
          snap: false,
          floating: false,
          collapsedHeight: 150,
          expandedHeight: 150,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(0.0),
            child: SearchBox(),
          ),
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const DetailsPage()),
                );
              },
              child: ProductCard(
                itemIndex: index,
                product: products[index],
              ));
        }, childCount: products.length))
      ],
    );
  }
}
